<?php

require 'html.php';

function rw($str, $rules) {
  $tmp = $str;
  foreach($rules as $old => $new) {
    $tmp = str_replace($old,$new,$tmp);
  }
  return $tmp;
}

// $tag => div a? $tag
function tag($opt) {
  $tag = $opt['tagline'];
  if (isset($tag) && $tag != '') {
    return
      div(array('class'=>'sociable_tagline'),
          (isset($opt['help_grow']) && !empty($opt['help_grow']))
          ? a(array('class'=>'sociable_tagline',
                    'target'=>'_blank',
                    'href'=>'http://blogplay.com',
                    'style'=>(($opt['icon_size']==16)
                              ?"font-size:11px;":"")
                    ."color:#333333;text-decoration:none"),
              $tag)
          :$tag);
  } else {
    return '';
  }
}

function url($site,$rw) {
  $url = (isset($site['script'])) ? $site['script'] : $site['url'];
  return rw($url, $rw);
}

// predicates to filter displays
function is_counter($site,$known) {
  return isset($sociable_known_sites[$sitename]["counter"]);
}
function is_key($k,$a) {
  return array_key_exists($sitename, $active_sites);
}
function active_not_counter($s,$ss,$known) {
  return !is_key($s,$ss) || is_counter($s,$known);
}
function display_not_counter($s,$ss,$known) {
  return is_key($s,$ss) || is_counter($s,$known);
}
function counter($s,$ss,$known) {
  return !is_key($s,$ss) && is_counter($s,$known);
}

/*
 * The Output And Shortcode Functions For sociable
 */
function send_config_sociable(){
  global $sociable_options;
  if (!empty($sociable_options["pixel"])){
    $posts = array();
    $posts["blog_name"] = get_bloginfo();
    $posts["blog_url"] = get_bloginfo('wpurl');
    $posts["admin_email"] = get_bloginfo('admin_email');
    $posts["language"] = get_bloginfo('language');
    $posts["version"] = get_bloginfo('version');
    $posts["blog_config"] = $sociable_options;
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, "http://sociablepixel.blogplay.com/index.php");
    curl_setopt($curl, CURLOPT_POST,true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, "sociable=1&info=".json_encode($posts)."&blog_url=".get_bloginfo('wpurl'));
    curl_setopt($curl, CURLOPT_HEADER ,0);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER ,0);
    $response = curl_exec ($curl);
    curl_close($curl);
  }
}

/*
 * Returns The Sociable Output For The Global $post Object Do Not
 */
function sociable_html($display = array(),$location = ""){
  global $sociable_options, $wp_query, $post;

  // less is more
  $opts = $sociable_options;
  
  if (!empty($opts["pixel"])){
    send_config_sociable();
  }

  if(!$post){
    $post = get_post($post_id = 1);
  }
  if (get_post_meta($post->ID,'_sociableoff',true)) {
    return "";
  }

  /*** SITES ***/
  $active = $opts['active_sites'];
  $known = get_option('sociable_known_sites');

  if (empty($display) && isset($active) && !empty($active)) {
    $display = $active;
    $display["More"] = "On";
  } else {
    return "";
  }

  // 0.CONSTANTS
  $lid = $location.'-'.$post->ID;
  $blogname = urlencode(get_bloginfo('name')." ".get_bloginfo('description'));
  $blogrss = get_bloginfo('rss2_url');
  $rss = urlencode(get_bloginfo('ref_url'));

  $permalinkCOUNT = get_permalink($post->ID);
  $permalink = urlencode($permalinkCOUNT);
  $titleCOUNT = $post->post_title;
  $title = str_replace('+','%20',urlencode($titleCOUNT));

  // excerpt or first 250 of post
  $excerpt = ($post->post_excerpt) ? $post->post_excerpt : $post->post_content;
  $excerpt = urlencode(substr(strip_tags(strip_shortcodes($excerpt)),0,250));
  $excerpt = str_replace('+','%20',$excerpt);

  $mgns = array("16" => "padding-top: 0;margin-top:-2px",
                "32" => "margin-top:9px",
                "48" => "margin-top:24px",
                "64" => "margin-top:38px");
  $margin = $mgns[$opts['icon_size']]
    ? $mgns[$opts['icon_size']]
    : "0px";

  $url_rw = array('TITLECOUNT' => $titleCOUNT,
                  'TITLE' => $title,
                  'RSS' => $rss,
                  'BLOGNAME' => $blogname,
                  'EXCERPT' => $excerpt,
                  'FEEDLINK' => $blogrss,
                  'PERMALINKCOUNT' => $permalinkCOUNT,
                  'PERMALINK' => $permalink);
  
  $def_at = array('style' => "cursor:pointer",
                  'rel' => "nofollow");

  // 1.LINKS INIT
  $links = "";
  foreach(array_filter('active_not_counter',$display) as $sitename) {
    $site = $known[$sitename];
    $url = url($site,$url_rw);
    if ($sitename == 'Twitter Counter' || $sitename== 'Twitter') {
      $rep = (isset($opts["linksoptions"]) && !empty($opts["linksoptions"]));
      $url = str_replace('SHARETAG',($rep) ? '*blogplay.com*' : '', $url);
    }
    $desc = (isset($site['description']) && $site['description'] != "")
      ? $site['description']
      : $sitename;

    /*
     * !image          => desc
     * !custom or more => get
     *  more           => <img more>
     */
    $linkitem = (!isset($opts['use_images']))
      ? $desc
      : (!empty($opts["custom_icons"]) || ($desc!= "More"))
        ? _get_sociable_image($site,$desc)
        : img(array('style'=> $margin,
                    'src' => SOCIABLE_HTTP_PATH."images/more.png"));
  
    $posX = $site["spriteCoordinates"][$opts['icon_size']]["0"];
    $posY = $site["spriteCoordinates"][$opts['icon_size']]["1"];
    $style = "background-position:".$posX." ".$posY;
    $target = isset($opts['new_window']) ? '_blank' : '' ;

    // link attrs and inner content
    if ($sitename == "Add to favorites" || $sitename=="More") {
      if ($sitename == "More") {
        $linki = $linkitem;
        $linka = array('onMouseOver' => "more(this,\'post'.$lid.'\')",
                       'onMouseOut' => "fixOnMouseOut(document.getElementById(\'sociable-post'.$lid.'\'), event, \'post'.$lid.'\')");
      } else {
        $linki = '';
        $linka = array('style' => 'cursor:pointer;'.$style,
                       'class' => $opts['icon_option'].'_'.$opts['icon_size'],
                       'title' => $sitename.'(not working in Chrome)',
                       'onClick' => $url);
      }
    } else {
      if($opts["icon_option"] == "option6" || !empty($opts["custom_icons"])) {
        $linki = $linkitem;
        $linka = array('target' => $target,
                       'style' => $desc.$opts['icon_size'].'_'
                       .str_replace("option","",$opts['icon_option']),
                       'title' => $sitename,
                       'href' => $url);
      } else {
        if (count(split("Counter",$sitename))>1) {
          $linki = $href;
          $linka = null;
        }else{
          $linki = '';
          $linka = array('target' => $target,
                         'style' => $style,
                         'class' => $opts['icon_option'].'_'.$opts['icon_size'],
                         'title' => $sitename,
                         'href' => $url);
        }
      }
    }
    // APPEND
    $links .=
      apply_filters('sociable_link',
                    '<li>'.(($linka)?a($linka+$def_at,$linki):$linki).'</li>');
  }
  // WRAP
  $links = '<ul class="clearfix">'.$links.'</ul>';

  // 2.INNER INIT
  $inner = "";
  foreach(array_filter('display_not_counter',$known) as $sitename){
    $site = $known[$sitename];
    $url = url($site,$url_rw);
    $linkitem = (!isset($opts['use_images']))
                 ? $desc
                 : (!empty($opts["custom_icons"]) || ($desc!="More"))
                    ? _get_sociable_image($site,$desc)
                    : "<img "
                      ."style= ".$margin."' "
                      ."src=".SOCIABLE_HTTP_PATH."images/more.png'>";

    $posX = $site["spriteCoordinates"][$opts['icon_size']]["0"];
    $posY = $site["spriteCoordinates"][$opts['icon_size']]["1"];
    $style = "background-position:".$posX." ".$posY;
    $target = isset($opts['new_window']) ? '_blank' : '';

    // link attrs and inner content = f sitename opts
    if ($sitename == "Add to favorites" || $sitename=="More") {
      if ($sitename == "More") {
        $linki = $linkitem;
        $linka = array('onMouseOver' => "more(this,\'post'".$lid.'\')"');
      } else {
        $linki = '';
        $linka = array('class' => $opts['icon_option'].'_'.$opts['icon_size'],
                       'style' => 'cursor:pointer;'.$style,
                       'title' => $sitename.' - not working in Chrome',
                       'onClick' => $url);
      }
    } else {
      if($opts["icon_option"] == "option6" || !empty($opts["custom_icons"])) {
        $linki = $linkitem;
        $linka = array('style' => $opts['icon_size'].'_'
                       .str_replace("option","",$opts['icon_option']),
                       'target' => $target,
                       'title' => $sitename,
                       'href' => $url);
      } else {
        $linki = '';
        $linka = array('class' => $opts['icon_option'].'_'.$opts['icon_size'],
                       'style' => $style,
                       'target' => $target,
                       'title' => $sitename,
                       'href' => $url);
      }
    }
    // APPEND
    $css = 'heigth:'.$opts['icon_size'].'px;width:'.$opts['icon_size'].'px';
    $inner .= apply_filters('sociable_link', li(array('style' => $css),
                                                a($linka+$def_at,$linki)));
  }
  // WRAP
  $inner .= "<ul>".$inner."</ul>";

  // 3.COUNTER INIT
  $counters = "";
  foreach(array_filter('counter',$display) as $sitename){
    $site = $known[$sitename];
    $url = url($site,$url_rw);
    $id = rw($sitename,array('+'=>'p',' '=>'_'));
    // APPEND
    $counters .= apply_filters('sociable_link', li(array('id'=>$id),$url));
  }
  // WRAP
  $counters = '
<div>
  <style>
    #sociable-post'.$lid.' { display: none; }
    #sociable { top:auto; left:auto; display:block; }
    #sociable a.close { cursor:pointer; }
  </style>
  <div onMouseout="fixOnMouseOut(this,event,\'post'.lid.'\')"
       id="sociable-post'.$lid.'>
           <div id="sociable">
             <div class="popup">
               <div class="content">'.$inner.'</div>
               <a onclick="hide_sociable(\'post'.$lid.'\',true)"
                  class="close">
	         <img title="close"
                      src="'.SOCIABLE_HTTP_PATH.'images/closelabel.png"
                      onclick="hide_sociable(\'post'.$lid.'\',true)">
               </a>
             </div>
           </div>
  </div>
  <ul class="clearfix">'
    .$counters
.'</ul>
</div>';

  $html = '<!-- Start Sociable --><div class="sociable">';
  $html .= tag($opts).$links.$counters;
  $html .= '<!-- End Sociable -->';
  return $html;
}

/*
 * Template Tag To Echo The Sociable 2 HTML
 */
function do_sociable(){
  echo  sociable_html();
}

/*
 * Hook For the_content to automatically output the sociable HTML If
 * The Option To Disable Has Not Been Unchecked
 */
function auto_sociable($content){
  global $sociable_options;
  $opts = $sociable_options;
  if (!isset($opts["active"])){
    return $content;
  }
  if(! isset($opts['locations'])
      || ! is_array($opts['locations'])
      || empty($opts['locations'])){
    return $content;
  } else {
    $locations = $opts['locations'];
  }

  /*
   * Determine if we are supposed to be displaying the output here.
   */
  $display = false;

  /*
   * is_single is a unique case it still returning true
   */
  //If We Can Verify That We are in the correct loaction, simply add
  //something to the $display array, and test for a true result to
  //continue.
  foreach($locations as $location => $val) {
    //First We Handle is_single() so it returning true on Single Post
    //Type Pages is not an issue, this is not the intended
    //functionality of this plugin
    if($location == 'is_single') {
      //If we are not in a post, lets ignore this one for now
      if(is_single()&& get_post_type()== 'post'){
        $display = true;
        break;
      } else {
        // So not to trigger is_single later in this loop,
        // but still be allowed to handle others
        continue; 
      }
    } elseif(strpos($location,'is_single_posttype_')=== 0){
      //Now We Need To Check For The Variable Names, Taxonomy
      //Archives, Post Type Archives and Single Custom Post Types.
      //Single Custom Post Type
      $post_type = str_replace('is_single_posttype_','',$location);
      if(is_single()&& get_post_type()==$post_type){
        $display = true;
        break;
      }
    } elseif(strpos($location,'is_posttype_archive_')=== 0){
      //Custom Post Type Archive
      $post_type = str_replace('is_posttype_archive_','',$location);
      if(is_post_type_archive($post_type)){
        $display = true;
        break;
      }
    } elseif(strpos($location,'is_taxonomy_archive_')=== 0){
      //Taxonomy Archive
      $taxonomy = str_replace('is_taxonomy_archive_','',$location);
      if(is_tax($taxonomy)){
        $display = true;
        break;
      }
    } elseif(function_exists($location)){
      //Standard conditional tag,these will return BOOL
      if(call_user_func($location)=== true){
        $display = true;
        break;
      }
    } else {
      continue;
    }
  }

  //If We have passed all the checks and are looking in the right
  //place lets do this thang
  if(isset($opts['automatic_mode']) && $display === true){
    if(isset($opts["topandbottom"])){
      $content = sociable_html(array(),"top")
                 .$content
                 .sociable_html(array(),"bottom");
    }else{
      $content = "".$content.sociable_html(array());
    }
  }
  return $content;
}

/*
 * Sociable 2 Shortcode
 */
function sociable_shortcode(){
  return sociable_html();
}
function add_googleplus() {
  //echo'<script
  // type="text/javascript"
  // src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js">
  //</script>';
}
//add_action('wp_head', 'add_googleplus' );
?>
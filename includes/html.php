<?php

/* HTML helpers? */
function H($n,$a,$i) {
  foreach($a as $an => $av) {
    $as[] = $an.'="'.$av.'"';
  }
  $has = implode(" ",$as);
  return (empty($i) || $i == '')
    ? "<$n $has/>"
    : "<$n $has>$i</$n>";
}

function a($a,$i)    { return H('a'       ,$a,$i); }
function div($a,$i)  { return H('div'     ,$a,$i); }
function ul($a,$i)   { return H('ul'      ,$a,$i); }
function li($a,$i)   { return H('li'      ,$a,$i); }
// singletons
function area($a)    { return H('area'    ,$a,''); }
function base($a)    { return H('base'    ,$a,''); }
function br($a)      { return H('br'      ,$a,''); }
function col($a)     { return H('col'     ,$a,''); }
function command($a) { return H('command' ,$a,''); }
function embed($a)   { return H('embed'   ,$a,''); }
function hr($a)      { return H('hr'      ,$a,''); }
function img($a)     { return H('img'     ,$a,''); }
function input($a)   { return H('input'   ,$a,''); }
function keygen($a)  { return H('keygen'  ,$a,''); }
function lnk($a)     { return H('link'    ,$a,''); }
function meta($a)    { return H('meta'    ,$a,''); }
function param($a)   { return H('param'   ,$a,''); }
function source($a)  { return H('source'  ,$a,''); }
function track($a)   { return H('track'   ,$a,''); }
function wbr($a)     { return H('wbr'     ,$a,''); }

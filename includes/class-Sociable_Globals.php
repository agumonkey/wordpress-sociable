<?php
/*
 * Global Arrays For The sociable Plugin.
 */
class Sociable_Globals{

    function default_sites(){
      $json = 'sites.json';
      $sites = json_decode(file_get_contents($json),true);
      return apply_filters( 'sociable_known_sites' , $sites );
    }

    /*
     * Return The Non Default Registered Post Types
     */
    function sociable_get_post_types(){
        $args = array(
            'public'   => true,
            '_builtin' => false
        );
        $types = get_post_types( $args, 'objects' , 'and');
        return $types;
    }

    /*
     * Return The Custom Taxonomies
     */
    function sociable_get_taxonomies(){
        $args = array(
          'public'   => true,
          '_builtin' => false
        ); 
        $taxonomies = get_taxonomies( $args, 'objects' , 'and' );
        return $taxonomies;
    }
}
?>